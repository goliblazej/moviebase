﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieBase.Domain.Contracts;
using MovieBase.Domain.Models;

namespace MovieBase.Service.Controllers
{
    [Route("Movies")]
    [ProducesResponseType(500)]
    public class MoviesController : Controller
    {
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetAllMovies(string title = "", string genre = "", string sort = "")
        {
            var movies = await _movieService.GetMoviesAsync(title, genre, sort);
            return Ok(movies);
        }

        [HttpPost("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddMovie(string title, string genre)
        {
            MovieModel movie = new MovieModel { Title = title, Genre = genre };
            if (!TryValidateModel(movie))
                return BadRequest();

            await _movieService.AddMovieAsync(movie);

            return Ok();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetMovie(int id)
        {
            var movie = await _movieService.GetMovieAsync(id);
            return Ok(movie);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateMovie(int id, MovieModel movie)
        {
            await _movieService.UpdateMovieAsync(id, movie);
            return Ok();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            await _movieService.DeleteMovieAsync(id);
            return Ok();
        }

        [HttpPut("{movieId}/user/{userId}/score/{score}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> AddRate(int movieId, int userId, int score)
        {
            RateModel rate = new RateModel { MovieId = movieId, UserId = userId, Score = score };

            if (!TryValidateModel(rate))
                return BadRequest();

            await _movieService.InsertRateAsync(new RateModel {MovieId = movieId, UserId = userId, Score = score});

            return Ok();
        }

        [HttpGet("user/{userId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetUsersTopTen(int userId)
        {
            var topTen = await _movieService.GetUsersTopTenAsync(userId);
            return Ok(topTen);
        }
    }
}