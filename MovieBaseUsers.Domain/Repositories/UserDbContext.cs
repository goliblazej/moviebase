﻿using Microsoft.EntityFrameworkCore;
using MovieBaseUsers.Domain.Entities;

namespace MovieBaseUsers.Domain.Repositories
{
    public class UserDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public UserDbContext(DbContextOptions<UserDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().HasKey(m => new { m.UserId });
        }
    }
}

