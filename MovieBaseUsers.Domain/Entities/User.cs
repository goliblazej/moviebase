﻿using System.ComponentModel.DataAnnotations;

namespace MovieBaseUsers.Domain.Entities
{
    public class User
    {
        [Range(1, int.MaxValue)]
        public int UserId { get; set; }

        [Required]
        public string Login { get; set; }
        
        public string Name { get; set; }
        
        public string Surname { get; set; }
    }
}
