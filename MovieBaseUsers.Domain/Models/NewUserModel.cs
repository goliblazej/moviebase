﻿using System.ComponentModel.DataAnnotations;

namespace MovieBaseUsers.Domain.Models
{
    public class NewUserModel
    {
        [Required]
        public string Login { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }
    }
}