﻿using System;
using System.Threading.Tasks;
using MovieBaseUsers.Domain.Entities;
using MovieBaseUsers.Domain.Models;

namespace MovieBaseUsers.Domain.Contracts
{
    public interface IUserRepository : IDisposable
    {
        Task<User> GetUserAsync(int userId);
        Task InsertUserAsync(NewUserModel user);
        Task DeleteUserAsync(int userId);
    }
}
