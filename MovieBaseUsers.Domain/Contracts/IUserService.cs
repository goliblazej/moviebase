﻿using System.Threading.Tasks;
using MovieBaseUsers.Domain.Entities;
using MovieBaseUsers.Domain.Models;

namespace MovieBaseUsers.Domain.Contracts
{
    public interface IUserService
    {
        Task<User> GetUserAsync(int userId);
        Task AddUserAsync(NewUserModel user);
        Task DeleteUserAsync(int userId);
    }
}
