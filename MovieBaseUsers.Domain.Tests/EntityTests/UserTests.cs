﻿using System.ComponentModel.DataAnnotations;
using MovieBaseUsers.Domain.Entities;
using Xunit;

namespace MovieBaseUsers.Domain.Tests.EntityTests
{
    public class UserTests
    {
        [Theory]
        [InlineData(0, "login", false)] // Invalid UserId, Valid Login
        [InlineData(1, "", false)] // Valid UserId, Invalid Login
        [InlineData(1, "login", true)] // Valid UserId, Valid Login
        public void User_ForValues_ValidatesCorrectly(int userId, string login, bool expectedResult)
        {
            // Given
            var user = new User
            {
                UserId = userId,
                Login = login,
                Name = "name",
                Surname = "surname"
            };
            var validationContext = new ValidationContext(user);

            // When
            var result = Validator.TryValidateObject(user, validationContext, null, true);

            // Then
            Assert.Equal(expectedResult, result);
        }
    }
}