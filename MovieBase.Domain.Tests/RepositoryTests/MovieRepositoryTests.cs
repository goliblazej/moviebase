﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;
using MovieBase.Domain.Models;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Repositories;

namespace MovieBase.Domain.Tests.RepositoryTests
{
    public class MovieRepositoryTests
    {
        private readonly DbContextOptions<MovieDbContext> _options;

        public MovieRepositoryTests()
        {
            _options = new DbContextOptionsBuilder<MovieDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
        }

        #region GetMoviesAsync

        [Fact]
        public async void GetMoviesAsync_ForNoData_ReturnsEmptyListOfItems()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var items = await movieRepository.GetMoviesAsync();

                // Then
                Assert.NotNull(items);
                Assert.Empty(items);
            }
        }

        [Fact]
        public async void GetMoviesAsync_DataExsist_ReturnsListOfItems()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync();

                // Then
                Assert.Equal(16, new List<Movie>(movies).Count);
            }
        }

        [Fact]
        public async void GetMoviesAsync_TitleFilterSet_ReturnFilteredList()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync("Harry");

                // Then
                Assert.Equal(6, new List<Movie>(movies).Count);
            }
        }

        [Fact]
        public async void GetMoviesAsync_GenreFilterSet_ReturnFilteredList()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync("", "Action");

                // Then
                Assert.Equal(7, movies.Count());
            }
        }

        [Fact]
        public async void GetMoviesAsync_SortFilterSet_ReturnSortedByTitleList()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync("", "", "title");

                // Then
                Assert.Equal(16, movies.Count());
                Assert.Equal(0, movies.ToList().FindIndex(m => m.Title == "Barry Potter"));
                Assert.Equal(14, movies.ToList().FindIndex(m => m.Title == "Hotel"));
            }
        }

        [Fact]
        public async void GetMoviesAsync_SortFilterSet_ReturnSortedByTitleListDESC()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync("", "", "titleDESC");

                // Then
                Assert.Equal(16, movies.Count());
                Assert.Equal(0, movies.ToList().FindIndex(m => m.Title == "Zarry Potter"));
                Assert.Equal(1, movies.ToList().FindIndex(m => m.Title == "Hotel"));
                Assert.Equal(15, movies.ToList().FindIndex(m => m.Title == "Barry Potter"));
            }
        }

        [Fact]
        public async void GetMoviesAsync_SortFilterSet_ReturnSortedByGenreList()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync("", "", "genre");

                // Then
                Assert.Equal(16, movies.Count());
                Assert.Equal(0, movies.ToList().FindIndex(m => m.Genre == "Action"));
                Assert.Equal(7, movies.ToList().FindIndex(m => m.Genre == "Drama"));
                Assert.Equal(15, movies.ToList().FindIndex(m => m.Genre == "Horror"));
            }
        }

        [Fact]
        public async void GetMoviesAsync_SortFilterSet_ReturnSortedByGenreListDESC()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync("", "", "genreDESC");

                // Then
                Assert.Equal(16, movies.Count());
                Assert.Equal(9, movies.ToList().FindIndex(m => m.Genre == "Action"));
                Assert.Equal(1, movies.ToList().FindIndex(m => m.Genre == "Drama"));
                Assert.Equal(0, movies.ToList().FindIndex(m => m.Genre == "Horror"));
            }
        }

        [Fact]
        public async void GetMoviesAsync_SortFilterSet_ReturnSortedByRateList()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync("", "", "rate");

                // Then
                Assert.Equal(16, movies.Count());
                Assert.Equal(0, movies.ToList().FindIndex(m => m.AverageScore == 2.21m));
                Assert.Equal(3, movies.ToList().FindIndex(m => m.AverageScore == 3.21m));
                Assert.Equal(15, movies.ToList().FindIndex(m => m.AverageScore == 4.87m));
            }
        }

        [Fact]
        public async void GetMoviesAsync_SortFilterSet_ReturnSortedByRateListDESC()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movies = await movieRepository.GetMoviesAsync("", "", "rateDESC");

                // Then
                Assert.Equal(16, movies.Count());
                Assert.Equal(15, movies.ToList().FindIndex(m => m.AverageScore == 2.21m));
                Assert.Equal(12, movies.ToList().FindIndex(m => m.AverageScore == 3.21m));
                Assert.Equal(0, movies.ToList().FindIndex(m => m.AverageScore == 4.87m));
            }
        }

        #endregion

        #region GetMovieAsync

        [Fact]
        public async void GetMovieAsync_MovieExsist_ReturnsMovie()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movie = await movieRepository.GetMovieAsync(2);

                // Then
                Assert.NotNull(movie);
                Assert.Equal(2, movie.MovieId);
                Assert.Equal("Harry Potter II", movie.Title);
                Assert.Equal("Action", movie.Genre);
            }
        }

        [Fact]
        public async void GetMovieAsync_MovieDoesntExsist_ReturnsEmpty()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var movie = await movieRepository.GetMovieAsync(777);

                // Then
                Assert.Null(movie);
            }
        }

        #endregion

        #region InsertMovieAsync

        [Fact]
        public async void InsertMovieAsync_ForNewMovie_InsertNewMovie()
        {
            // Given
            var movie = new MovieModel { Title = "Harry Potter", Genre = "Action"};

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.InsertMovieAsync(movie);

                // Then
                var newMovie = await context.Movies.SingleAsync(i => i.Title == "Harry Potter");
                Assert.NotNull(newMovie);
                Assert.Equal("Harry Potter", newMovie.Title);
                Assert.Equal("Action", newMovie.Genre);
            }
        }

        [Fact]
        public async void InsertMovieAsync_ForTitleMissing_DontInsertNewMovie()
        {
            // Given
            var movie = new MovieModel { Title = "", Genre = "Action" };

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.InsertMovieAsync(movie);

                // Then
                var newMovie = await context.Movies.SingleOrDefaultAsync(i => i.Title == "Harry Potter");
                Assert.Null(newMovie);
            }
        }

        [Fact]
        public async void InsertMovieAsync_ForGenreMissing_DontInsertNewMovie()
        {
            // Given
            var movie = new MovieModel { Title = "Harry Potter", Genre = "" };

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.InsertMovieAsync(movie);

                // Then
                var newMovie = await context.Movies.SingleOrDefaultAsync(i => i.Genre == "Action");
                Assert.Null(newMovie);
            }
        }

        #endregion

        #region DeleteMovieAsync

        [Fact]
        public async void DeleteMovieAsync_MovieIdExist_DeleteMovie()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var shoppingCartRepository = new MovieRepository(context))
            {
                // When
                await shoppingCartRepository.DeleteMovieAsync(1);

                // Then
                var moviesCount = context.Movies.Count();
                Assert.Equal(15, moviesCount);
            }
        }

        [Fact]
        public async void DeleteMovieAsync_MovieIdDoesntExist_MovieNotDeleted()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                await movieRepository.DeleteMovieAsync(6);

                // Then
                var moviesCount = context.Movies.Count();
                Assert.Equal(15, moviesCount);
            }
        }

        #endregion

        #region UpdateMovieAsync

        [Fact]
        public async void UpdateMovieAsync_MovieExist_MovieEdited()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            var movie = new MovieModel {Title = "UpdatedMovie", Genre = "Comedy"};

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                var oldMovie = movieRepository.GetMovieAsync(2);
                await movieRepository.UpdateMovieAsync(2, movie);

                // Then
                var newMovie = await context.Movies.SingleAsync(m => m.MovieId == 2);
                Assert.NotNull(newMovie);
                Assert.Equal(2, newMovie.MovieId);
                Assert.Equal("UpdatedMovie", newMovie.Title);
                Assert.Equal("Comedy", newMovie.Genre);
            }
        }

        [Fact]
        public async void UpdateMovieAsync_MovieDoesntExist_MovieNotEdited()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.SaveChangesAsync();
            }

            var movie = new MovieModel { Title = "UpdatedMovie", Genre = "Comedy" };

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                await movieRepository.UpdateMovieAsync(20, movie);

                // Then
                var newMovie = await context.Movies.SingleOrDefaultAsync(m => m.MovieId == 20);
                
                Assert.Null(newMovie);
            }
        }
        #endregion

        #region UpdateAverageScoreAsync

        [Fact]
        public async void UpdateAverageScoreAsync_ForFirstRating_SetRateToGivenRate()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.Movies.AddAsync(new Movie
                {
                    MovieId = 17,
                    Title = "Counter",
                    Genre = "Document",
                    AverageScore = 0
                });
                await context.Rates.AddAsync(new Rate
                {
                    MovieId = 17,
                    Score = 4,
                    UserId = 1
                });
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                await movieRepository.UpdateAverageScoreAsync(17);

                // Then
                var movie = await context.Movies.SingleOrDefaultAsync(m => m.MovieId == 17);

                Assert.Equal(4, movie.AverageScore);
            }
        }

        [Fact]
        public async void UpdateAverageScoreAsync_ForNextRating_CalculateAverageRate()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.Movies.AddAsync(new Movie
                {
                    MovieId = 17,
                    Title = "Counter",
                    Genre = "Document",
                    AverageScore = 0
                });
                await context.Rates.AddAsync(new Rate
                {
                    MovieId = 17,
                    Score = 4,
                    UserId = 1
                });
                await context.Rates.AddAsync(new Rate
                {
                    MovieId = 17,
                    Score = 3,
                    UserId = 2
                });
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                await movieRepository.UpdateAverageScoreAsync(17);

                // Then
                var movie = await context.Movies.SingleOrDefaultAsync(m => m.MovieId == 17);

                Assert.Equal(3.5m, movie.AverageScore);
            }
        }

        [Fact]
        public async void UpdateAverageScoreAsync_ForLastRateDeleted_AverageComesBackToZero()
        {
            // Given
            using (var context = new MovieDbContext(_options))
            {
                BuildMovieContext(context);
                await context.Movies.AddAsync(new Movie
                {
                    MovieId = 17,
                    Title = "Counter",
                    Genre = "Document",
                    AverageScore = 0
                });
                await context.Rates.AddAsync(new Rate
                {
                    MovieId = 17,
                    Score = 4,
                    UserId = 1
                });
                await context.SaveChangesAsync();
            }

            using (var context = new MovieDbContext(_options))
            using (var movieRepository = new MovieRepository(context))
            {
                // When
                await movieRepository.UpdateAverageScoreAsync(17);
                var scoreBefore = (await context.Movies.SingleOrDefaultAsync(m => m.MovieId == 17)).AverageScore;
                // Then
                context.Rates.Remove(context.Rates.Last());
                await context.SaveChangesAsync();
                await movieRepository.UpdateAverageScoreAsync(17);
                var scoreAfter = (await context.Movies.SingleOrDefaultAsync(m => m.MovieId == 17)).AverageScore;

                Assert.Equal(4, scoreBefore);
                Assert.Equal(0, scoreAfter);
            }
        }

#endregion

        #region TestHelpers

        private void BuildMovieContext(MovieDbContext context)
        {
            context.Movies.Add(new Movie { MovieId = 1, Title = "Harry Potter", Genre = "Action", AverageScore = 4.87m });
            context.Movies.Add(new Movie { MovieId = 2, Title = "Harry Potter II", Genre = "Action", AverageScore = 4.86m });
            context.Movies.Add(new Movie { MovieId = 3, Title = "Harry Potter III", Genre = "Action", AverageScore = 4.85m });
            context.Movies.Add(new Movie { MovieId = 4, Title = "Harry Potter IV", Genre = "Action", AverageScore = 4.84m });
            context.Movies.Add(new Movie { MovieId = 5, Title = "Zarry Potter", Genre = "Action", AverageScore = 4.83m });
            context.Movies.Add(new Movie { MovieId = 6, Title = "Harry Potter V", Genre = "Action", AverageScore = 4.82m });
            context.Movies.Add(new Movie { MovieId = 7, Title = "Harry Potter VI", Genre = "Action", AverageScore = 4.81m });
            context.Movies.Add(new Movie { MovieId = 8, Title = "Barry Potter", Genre = "Horror", AverageScore = 4.80m });
            context.Movies.Add(new Movie { MovieId = 9, Title = "Dr House", Genre = "Drama", AverageScore = 3.29m });
            context.Movies.Add(new Movie { MovieId = 10, Title = "Dr House II", Genre = "Drama", AverageScore = 3.21m });
            context.Movies.Add(new Movie { MovieId = 11, Title = "Dr House III", Genre = "Drama", AverageScore = 3.87m });
            context.Movies.Add(new Movie { MovieId = 12, Title = "Dr House IV", Genre = "Drama", AverageScore = 2.45m });
            context.Movies.Add(new Movie { MovieId = 13, Title = "Dr House V", Genre = "Drama", AverageScore = 4.44m });
            context.Movies.Add(new Movie { MovieId = 14, Title = "Dr House VI", Genre = "Drama", AverageScore = 2.21m });
            context.Movies.Add(new Movie { MovieId = 15, Title = "Dr House VII", Genre = "Drama", AverageScore = 2.28m });
            context.Movies.Add(new Movie { MovieId = 16, Title = "Hotel", Genre = "Drama", AverageScore = 3.97m });

            context.Rates.Add(new Rate { MovieId = 1, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 2, UserId = 2, Score = 3 });
            context.Rates.Add(new Rate { MovieId = 3, UserId = 2, Score = 3 });
            context.Rates.Add(new Rate { MovieId = 4, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 5, UserId = 2, Score = 2 });
            context.Rates.Add(new Rate { MovieId = 6, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 7, UserId = 2, Score = 2 });
            context.Rates.Add(new Rate { MovieId = 8, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 9, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 10, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 11, UserId = 2, Score = 5 });
            context.Rates.Add(new Rate { MovieId = 12, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 13, UserId = 2, Score = 5 });
            context.Rates.Add(new Rate { MovieId = 14, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 15, UserId = 2, Score = 4 });
            context.Rates.Add(new Rate { MovieId = 16, UserId = 2, Score = 5 });
        }

        #endregion
    }
}