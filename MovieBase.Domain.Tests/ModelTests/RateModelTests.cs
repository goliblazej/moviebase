﻿using System.ComponentModel.DataAnnotations;
using MovieBase.Domain.Models;
using Xunit;

namespace MovieBase.Domain.Tests.ModelTests
{
    public class RateModelTests
    {
        [Theory]
        [InlineData(0, 1, 4, false)] // Invalid movieId, Valid userId, Valid score
        [InlineData(1, 0, 4, false)] // Valid movieId, Invalid userId, Valid score
        [InlineData(1, 1, 0, false)] // Valid movieId, Valid userId, Invalid score
        [InlineData(1, 1, 6, false)] // Valid movieId, Valid userId, Invalid score
        [InlineData(1, 1, 5, true)] // Valid movieId, Valid userId, Valid score
        public void RateModel_ForValues_ValidatesCorrectly(int movieId, int userId, int score, bool expectedResult)
        {
            // Given
            var rateModel = new RateModel
            {
                MovieId = movieId,
                UserId = userId,
                Score = score
            };
            var validationContext = new ValidationContext(rateModel);

            // When
            var result = Validator.TryValidateObject(rateModel, validationContext, null, true);

            // Then
            Assert.Equal(expectedResult, result);
        }
    }
}
