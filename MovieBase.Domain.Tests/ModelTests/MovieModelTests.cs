﻿using System.ComponentModel.DataAnnotations;
using MovieBase.Domain.Models;
using Xunit;

namespace MovieBase.Domain.Tests.ModelTests
{
    public class MovieModelTests
    {
        [Theory]
        [InlineData("", false)] // Invalid title
        [InlineData("title", true)] // Valid title
        public void MovieModel_ForValues_ValidatesCorrectly(string title, bool expectedResult)
        {
            // Given
            var movieModel = new MovieModel
            {
                Title = title,
                Genre = ""
            };
            var validationContext = new ValidationContext(movieModel);

            // When
            var result = Validator.TryValidateObject(movieModel, validationContext, null, true);

            // Then
            Assert.Equal(expectedResult, result);
        }
    }
}
