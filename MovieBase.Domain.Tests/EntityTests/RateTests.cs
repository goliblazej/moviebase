﻿using System.ComponentModel.DataAnnotations;
using MovieBase.Domain.Entities;
using Xunit;

namespace MovieBase.Domain.Tests.EntityTests
{
    public class RateTests
    {
        [Theory]
        [InlineData(0, 1, 2, false)] // Invalid MovieId, Valid UserId, Valid Score
        [InlineData(1, 0, 0, false)] // Valid MovieId, Invalid UserId, Valid Score
        [InlineData(1, 1, 0, false)] // Valid MovieId, Valid UserId, Invalid Score
        [InlineData(1, 1, 6, false)] // Valid MovieId, Valid UserId, Invalid Score
        [InlineData(1, 1, 2, true)] // Valid MovieId, Valid UserId, Valid Score
        public void Rate_ForValues_ValidatesCorrectly(int movieId, int userId, int score, bool expectedResult)
        {
            // Given
            var rate = new Rate
            {
                MovieId = movieId,
                UserId = userId,
                Score = score
            };
            var validationContext = new ValidationContext(rate);

            // When
            var result = Validator.TryValidateObject(rate, validationContext, null, true);

            // Then
            Assert.Equal(expectedResult, result);
        }
    }
}