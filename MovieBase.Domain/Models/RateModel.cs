﻿using System.ComponentModel.DataAnnotations;

namespace MovieBase.Domain.Models
{
    public class RateModel
    {
        [Range(1, int.MaxValue)]
        public int MovieId { get; set; }
        [Range(1, int.MaxValue)]
        public int UserId { get; set; }
        [Range(1, 5)]
        public int Score { get; set; }
    }
}
