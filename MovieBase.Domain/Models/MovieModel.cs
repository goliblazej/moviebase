﻿using System.ComponentModel.DataAnnotations;

namespace MovieBase.Domain.Models
{
    public class MovieModel
    {
        [Required]
        public string Title { get; set; }
        public string Genre { get; set; }
    }
}
