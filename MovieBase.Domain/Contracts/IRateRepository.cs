﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Contracts
{
    public interface IRateRepository : IDisposable
    {
        Task InsertRateAsync(RateModel rate);
        Task<List<int>> GetUsersTopTenAsync(int userId);
    }
}