﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Contracts
{
    public interface IMovieRepository : IDisposable
    {
        Task<IEnumerable<Movie>> GetMoviesAsync(string title = "", string genere = "", string sort = "");
        Task<Movie> GetMovieAsync(int movieId);
        Task InsertMovieAsync(MovieModel movie);
        Task UpdateMovieAsync(int movieId, MovieModel movie);
        Task DeleteMovieAsync(int movieId);
        Task UpdateAverageScoreAsync(int movieId);
    }
}