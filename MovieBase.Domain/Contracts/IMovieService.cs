﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Contracts
{
    public interface IMovieService
    {
        Task<IEnumerable<Movie>> GetMoviesAsync(string title = "", string genre = "", string sort = "");
        Task<Movie> GetMovieAsync(int movieId);
        Task AddMovieAsync(MovieModel movie);
        Task UpdateMovieAsync(int movieId, MovieModel movie);
        Task DeleteMovieAsync(int movieId);
        Task InsertRateAsync(RateModel rate);
        Task<List<int>> GetUsersTopTenAsync(int userId);
    }
}
