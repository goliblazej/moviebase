﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Contracts;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieDbContext _context;

        public MovieRepository(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Movie>> GetMoviesAsync(string title = "", string genre = "", string sort = "")
        {
            var movies =  await _context.Movies.ToListAsync();

            if (title != "")
                movies = movies.Where(m => m.Title.Contains(title)).ToList();

            if (genre != "")
                movies = movies.Where(m => m.Genre == genre).ToList();

            if (sort != "")
            {
                switch (sort)
                {
                    case "title":
                    {
                        movies = movies.OrderBy(m => m.Title).ToList();
                        break;
                    }
                    case "titleDESC":
                    {
                        movies = movies.OrderByDescending(m => m.Title).ToList();
                        break;
                    }
                    case "genre":
                    {
                        movies = movies.OrderBy(m => m.Genre).ToList();
                        break;
                    }
                    case "genreDESC":
                    {
                        movies = movies.OrderByDescending(m => m.Genre).ToList();
                        break;
                    }
                    case "rate":
                    {
                        movies = movies.OrderBy(m => m.AverageScore).ToList();
                        break;
                    }
                    case "rateDESC":
                    {
                        movies = movies.OrderByDescending(m => m.AverageScore).ToList();
                        break;
                    }
                }
            }

            return movies;
        }

        public async Task<Movie> GetMovieAsync(int movieId)
        {
            return await _context.Movies.Where(m => m.MovieId == movieId).FirstOrDefaultAsync();
        }

        public async Task InsertMovieAsync(MovieModel movie)
        {
            await _context.Movies.AddAsync(new Movie
            {
                MovieId = await GetNewAvailableId(),
                Title = movie.Title,
                Genre = movie.Genre,
                AverageScore = 0.0m
            });
            await _context.SaveChangesAsync();
        }

        private async Task<int> GetNewAvailableId()
        {
            var movieMaxId = await _context.Movies.OrderByDescending(m => m.MovieId).FirstOrDefaultAsync();

            if (movieMaxId == null)
                return 1;

            return movieMaxId.MovieId + 1;
        }

        public async Task UpdateMovieAsync(int movieId, MovieModel movie)
        {
            var existingItem = await _context.Movies.Where(m => m.MovieId == movieId).FirstOrDefaultAsync();

            if (existingItem != null)
            {
                existingItem.Title = movie.Title;
                existingItem.Genre = movie.Genre;
                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteMovieAsync(int movieId)
        {
            var movieToRemove =  _context.Movies.Find(movieId);
            if (movieToRemove != null)
            {
                _context.Movies.Remove(movieToRemove);
                await _context.SaveChangesAsync();
            }
        }

        public async Task UpdateAverageScoreAsync(int movieId)
        {
            decimal result = 0;
            var rates = await _context.Rates.Where(r => r.MovieId == movieId).Select(r => r.Score).ToListAsync();
            
            if (rates.Count != 0)
            {
                int sum = 0;
                int counter = 0;
                foreach (var rate in rates)
                {
                    sum += rate;
                    counter++;
                }

                result = (decimal)sum / counter;
            }

            var existingItem = await _context.Movies.Where(m => m.MovieId == movieId).FirstOrDefaultAsync();
            if (existingItem != null)
            {
                existingItem.AverageScore = result;
                await _context.SaveChangesAsync();
            }
        }
        
        #region IDisposable

        private bool _disposed;

        ~MovieRepository()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _disposed = true;
                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion                               
    }
}
