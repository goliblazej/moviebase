﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Contracts;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Repositories
{
    public class RateRepository : IRateRepository
    {
        private readonly MovieDbContext _context;
        private readonly IMovieRepository _movieRepository;

        public RateRepository(MovieDbContext context, IMovieRepository movieRepository)
        {
            _context = context;
            _movieRepository = movieRepository;
        }

        public async Task InsertRateAsync(RateModel rate)
        {
            var movie = await _context.Movies.Where(m => m.MovieId == rate.MovieId).FirstOrDefaultAsync();

            if (movie != null)
            {
                var existingItem = await _context.Rates.Where(r => r.UserId == rate.UserId && r.MovieId == rate.MovieId).FirstOrDefaultAsync();

                if (existingItem != null)
                {
                    existingItem.Score = rate.Score;
                }
                else
                {
                    await _context.Rates.AddAsync(new Rate
                    {
                        UserId = rate.UserId,
                        MovieId = rate.MovieId,
                        Score = rate.Score
                    });
                }

                await _context.SaveChangesAsync();
                await _movieRepository.UpdateAverageScoreAsync(rate.MovieId);
            }
        }

        public async Task<List<int>> GetUsersTopTenAsync(int userId)
        {
            return await _context.Rates
                .Where(r => r.UserId == userId)
                .OrderByDescending(r => r.Score)
                .Select(r => r.MovieId)
                .Take(10).ToListAsync();
        }

        #region IDisposable

        private bool _disposed;

        ~RateRepository()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _disposed = true;
                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion
    }
}
