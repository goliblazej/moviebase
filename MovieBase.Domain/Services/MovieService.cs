﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieBase.Domain.Contracts;
using MovieBase.Domain.Entities;
using MovieBase.Domain.Models;

namespace MovieBase.Domain.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IRateRepository _rateRepository;

        public MovieService(IMovieRepository movieRepository, IRateRepository rateRepository)
        {
            _movieRepository = movieRepository;
            _rateRepository = rateRepository;
        }

        public async Task<IEnumerable<Movie>> GetMoviesAsync(string title = "", string genre = "", string sort = "")
        {
            return await _movieRepository.GetMoviesAsync(title, genre, sort);
        }

        public async Task AddMovieAsync(MovieModel movie)
        {
            await _movieRepository.InsertMovieAsync(movie);
        }

        public async Task<Movie> GetMovieAsync(int movieId)
        {
            return await _movieRepository.GetMovieAsync(movieId);
        }

        public async Task UpdateMovieAsync(int movieId, MovieModel movie)
        {
            await _movieRepository.UpdateMovieAsync(movieId, movie);
        }

        public async Task DeleteMovieAsync(int movieId)
        {
            await _movieRepository.DeleteMovieAsync(movieId);
        }

        public async Task InsertRateAsync(RateModel rate)
        {
            await _rateRepository.InsertRateAsync(rate);
        }

        public async Task<List<int>> GetUsersTopTenAsync(int userId)
        {
            return await _rateRepository.GetUsersTopTenAsync(userId);
        }
    }
}
